sdk_path = "/home/vagrant/android-sdk"
ndk_paths = {
    'r9b': "/home/vagrant/android-ndk/r9b",
    'r10e': "/home/vagrant/android-ndk/r10e",
}
java_paths = {
    '7': "/usr/lib/jvm/java-7-openjdk-i386",
    '8': "/usr/lib/jvm/java-8-openjdk-i386",
}
